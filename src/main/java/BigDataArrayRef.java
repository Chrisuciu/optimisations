/**
 * Created by nico on 2016-11-15.
 */
public class BigDataArrayRef
{
    protected int[] data;

    public BigDataArrayRef()
    {
        this.data = new int[3276800];
        initData();
    }

    public void setDataAt(int index, int data)
    {
        this.data[index] = data;
    }

    public int getDataAt(int index)
    {
        return this.data[index];
    }

    public int getSize()
    {
        return this.data.length;
    }
    private void initData()
    {
        for (int i = 0; i < data.length; i++) {
            this.data[i] = (int)(Math.random() * data.length);
        }
    }

}
