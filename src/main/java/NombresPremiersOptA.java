import java.util.ArrayList;
import java.util.List;

/**
 * Created by nico on 2016-11-15.
 *
 * Added break when a number is not prime
 */
public class NombresPremiersOptA extends Thread
{
    int MAX;
    List<Integer> listePremiers;

    public NombresPremiersOptA(int MAX)
    {
        this.MAX = MAX;
        listePremiers = new ArrayList<Integer>();
    }

    public void run()
    {
        long temps = calculePremiers();
        System.out.println(getClass().toString() + " " + listePremiers.size() + " nombres premiers trouvés en " + temps + " ms\n");
    }

    public boolean estPremier(int candidat)
    {
        boolean premier = true;
        for (int diviseur = 2; diviseur < candidat; diviseur++) {
            int resultat = candidat % diviseur;
            if (0 == resultat) {
                premier = false;
                break;
            }
        }
        return premier;
    }

    public long calculePremiers()
    {
        long debut = System.currentTimeMillis();
        for (int candidat=3; candidat < MAX; candidat++) {
            if (estPremier(candidat)) {
                listePremiers.add(candidat);
            }
        }
        return (System.currentTimeMillis() - debut);
    }

}
