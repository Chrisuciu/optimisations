import java.util.Scanner;

/**
 * Created by nico on 2016-11-15.
 */
public class BigDataArrayMain
{
    static BigDataArrayRef bigData;
    static BigDataArrayOpt bigDataOpt;

    public static void main(String[] args)
    {
        bigData = new BigDataArrayRef();
        bigDataOpt = new BigDataArrayOpt();

        /*
        System.out.println("Press [enter] to continue");
        Scanner scan = new Scanner(System.in);
        scan.nextLine();
        System.out.println("Starting!");
		*/
        
        long tempsRef = calculRef(12);
        System.out.println("total time REF  : " + tempsRef + " ms");

        long tempsOpt1 = calculOpt1(12);
        System.out.println("total time OPT-1: " + tempsOpt1 + " ms");

        long tempsOpt2 = calculOpt2(12);
        System.out.println("total time OPT-2: " + tempsOpt2 + " ms");

        long tempsOpt3 = bigDataOpt.calculOpt3(12);
        System.out.println("total time OPT-3: " + tempsOpt3 + " ms");
    }

    public static long calculRef(int hauteur)
    {
        long start = System.currentTimeMillis();

        // REF
        for (int i = 0; i < bigData.getSize(); i++) {
            int tmp = (int) (bigData.getDataAt(i) * (Math.PI * Math.cos(hauteur)));
            bigData.setDataAt(i, tmp);
        }

        long end = System.currentTimeMillis();
        return (end - start);
    }

    public static long calculOpt1(int hauteur)
    {
        long start = System.currentTimeMillis();

        // OPT-1
        double coeff = Math.PI * Math.cos(hauteur);
        for (int i = 0; i < bigData.getSize(); i++) {
            int tmp = (int) (bigData.getDataAt(i) * coeff);
            bigData.setDataAt(i, tmp);
        }

        long end = System.currentTimeMillis();
        return (end - start);
    }

    public static long calculOpt2(int hauteur)
    {
        long start = System.currentTimeMillis();

        // OPT-2
        double coeff = Math.PI * Math.cos(hauteur);
        int size = bigData.getSize();
        for (int i = 0; i < size; i++) {
            int tmp = (int) (bigData.getDataAt(i) * coeff);
            bigData.setDataAt(i, tmp);
        }

        long end = System.currentTimeMillis();
        return (end - start);
    }
}
